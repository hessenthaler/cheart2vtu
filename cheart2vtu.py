#/bin/python
#
# pvpython script to convert CHeart data to vtk unstructured grid format (vtu)
#
# author: Andreas Hessenthaler
#
import os.path
import numpy as np
import sys
from paraview.simple import XMLUnstructuredGridReader, XMLUnstructuredGridWriter

def main():
    ################################################################################################
    # Compressed binary VTU file format?
    useCompression = True
    ################################################################################################
    # print header
    print("################################################################################################")
    print("")
    print("    pvpython script to convert CHeart data to vtk unstructured grid format (vtu)")
    print("")
    print("    author: Andreas Hessenthaler")
    print("")
    print("################################################################################################")
    ################################################################################################
    # read commandline arguments
    if (len(sys.argv) < 9):
        print("")
        print("Missing or invalid commandline arguments:")
        print("")
        print("    pvpython cheart2vtu.py outfolder prefix start stop increment topologyname boundaryname infolder spacename (variablename1) (variablename2) (...)")
        print("")
        print("        outfolder        : folder name where vtu-files are dumped")
        print("        prefix           : file name prefix; file name at time index t will be prefix-t.vtu")
        print("        start            : first index")
        print("        stop             : last index")
        print("        increment        : increment")
        print("        topologyname     : *.T file for topology")
        print("        boundaryname     : *.B file for topology (ignored if set to none)")
        print("        infolder         : data folder name")
        print("        spacename        : file name of spatial variable")
        print("        (variablename1)  : file name of 1st variable (optional)")
        print("        (variablename2)  : file name of 2nd variable (optional)")
        print("        (...)            : file names of more variables (optional)")
        print("")
        return
    ################################################################################################
    # set file suffixes for VTU and CHeart (and possible compression)
    vtksuffix           = ".vtu"
    cheartsuffix        = ".D"
    gzsuffix            = ".gz"
    # get run path and add trailing slash if it's not already there
    dirPrefix           = os.getcwd()
    dirPrefix           = os.path.join(dirPrefix, "")
    # set output directory
    outfolder           = dirPrefix + str(sys.argv[1])
    outfolder           = os.path.join(outfolder, "")
    # set output file
    prefix              = str(sys.argv[2])
    outfile             = outfolder + prefix
    # get time series
    index0              = int(sys.argv[3])
    index1              = int(sys.argv[4])
    increment           = int(sys.argv[5])
    # get topology *.T file name and boundary *.B file name and set absolute paths
    topologyname        = dirPrefix + str(sys.argv[6])
    boundaryname        = dirPrefix + str(sys.argv[7])
    # get data folder name and set absolute path
    infolder            = dirPrefix + str(sys.argv[8])
    infolder            = os.path.join(infolder, "")
    # get variable names
    spacename           = infolder + str(sys.argv[9])
    deformedSpace       = spacename.split("+")
    if (len(deformedSpace) == 2):
        spacename       = deformedSpace[0]
    variablenames       = []
    for variablenameIdx in range(10, len(sys.argv), 1):
        variablenames.append(str(sys.argv[variablenameIdx]))
    ################################################################################################
    print("")
    print("<<< Time series:                 From "+str(index0)+" to "+str(index1)+" with increment "+str(increment))
    print("<<< Compressed VTU format:       "+str(useCompression))
    print("<<< Output folder:               "+str(outfolder))
    print("<<< Output file name prefix:     "+str(prefix))
    print("<<< Output file name (boundary): "+str(prefix)+"_boundary"+vtksuffix)
    print("")
    # pre-flight to check whether all files exist
    if not(os.path.isfile(topologyname)):
        print(">>>ERROR: File "+topologyname+" does not exist.")
        return
    if not(os.path.isfile(boundaryname)):
        print(">>>WARNING: File "+boundaryname+" does not exist.")
        print("")
    # loop over all requested times
    for time in range(index0, index1+1, increment):
        # check space variable
        if (spacename[-2:] == ".X"):
            xfile = spacename
        else:
            xfile = spacename + "-" + str(time) + cheartsuffix
        if not(os.path.isfile(xfile)):
            xfilegz = xfile + ".gz"
            if not(os.path.isfile(xfilegz)):
                print(">>>ERROR: File "+xfile+" does not exist.")
                return
        # check other variables
        for varIdx in range(0, len(variablenames), 1):
            vfile = infolder + str(variablenames[varIdx]) + "-" + str(time) + ".D"
            if not(os.path.isfile(vfile)):
                vfilegz = vfile + ".gz"
                if not(os.path.isfile(vfilegz)):
                    if (len(deformedSpace) == 2):
                        if (deformedSpace[1] == variablenames[varIdx]):
                            print(">>>ERROR: File "+vfile+" does not exist.")
                            return
                    print(">>>WARNING: File "+vfile+" does not exist.")
                    print("")
    ################################################################################################
    # read topology and get number of elements, number of nodes per elements
    ft  = np.loadtxt(topologyname, skiprows=1, dtype=int)
    fen = ft.shape[0]
    fcn = ft.shape[1]
    # get VTK element type
    # bilinear triangle
    if (fcn == 3):
        vtkelementtype = 5
        vtksurfacetype = 3
    # biquadratic triangle
    elif (fcn == 6):
        vtkelementtype = 22
        vtksurfacetype = 21
    # bilinear quadrilateral / trilinear tetrahedron
    elif (fcn == 4):
        with open(boundaryname, "r") as f:
            first_line = f.readline().strip()
            second_line = map(int, f.readline().strip().split())
        # trilinear tetrahedron
        if (len(second_line) == 5):
            vtkelementtype = 10
            vtksurfacetype = 5
        # bilinear quadrilateral
        elif (len(second_line) == 4):
            vtkelementtype = 9
            vtksurfacetype = 3
    # biquadratic quadrilateral
    elif (fcn == 9):
        vtkelementtype = 28
        vtksurfacetype = 21
    # triquadratic tetrahedron
    elif (fcn == 10):
        vtkelementtype = 24
        vtksurfacetype = 22
    # trilinear hexahedron
    elif (fcn == 8):
        vtkelementtype = 12
        vtksurfacetype = 9
    # triquadratic hexahedron
    elif (fcn == 27):
        vtkelementtype = 29
        vtksurfacetype = 28
    else:
        print(">>>ERROR: Element type not implemented.")
        return
    ################################################################################################
    # export patch IDs from B-file
    # for boundary coordinates, let's default to first time step
    time = index0
    # read boundary and get number of elements, number of nodes per elements
    fb      = np.loadtxt(boundaryname, skiprows=1, dtype=int)
    fben    = fb.shape[0]
    fbcn    = fb.shape[1]
    # write header
    foutfile = outfile+"_boundary"+vtksuffix
    fout = open(foutfile, "w")
    fout.write("<VTKFile type=\"UnstructuredGrid\">\n")
    fout.write("  <UnstructuredGrid>\n")
    # convert space variable
    if (spacename[-2:] == ".X"):
        xfile = spacename
    else:
        xfile = spacename + "-" + str(time) + cheartsuffix
    if not(os.path.isfile(xfile)):
            xfile = xfile + ".gz"
    fx  = np.loadtxt(xfile,  skiprows=1, dtype=float)
    fnn = fx.shape[0]
    fnd = fx.shape[1]
    # if deformed space, then add displacement
    if (len(deformedSpace) == 2):
        dfile   = infolder + deformedSpace[1] + "-" + str(time) + ".D"
        if not(os.path.isfile(dfile)):
            dfile = dfile + ".gz"
        fd  = np.loadtxt(dfile, skiprows=1, dtype=float)
        fx  = fx + fd
    # VTU files are defined in 3D space, so we have to append a zero column for 2D data
    if (fnd == 1):
        print(">>>ERROR: Cannot convert data that lives on 1D domains.")
        return
    elif (fnd == 2):
        z   = np.zeros((fnn,1), dtype=float)
        fx  = np.append(fx, z, axis=1)
    fout.write("    <Piece Name=\""+prefix+"\" NumberOfPoints=\""+str(fnn)+"\" NumberOfCells=\""+str(fben)+"\">\n")
    fout.write("      <Points>\n")
    fout.write("        <DataArray type=\"Float64\" NumberOfComponents=\"3\" Format=\"ascii\">\n")
    for points in range(fx.shape[0]):
        fout.write("          % .16f % .16f % .16f\n" % (fx[points, 0], fx[points, 1], fx[points, 2]))
    fout.write("        </DataArray>\n")
    fout.write("      </Points>\n")
    fout.write("      <CellData Scalars=\"scalars\">\n")
    fout.write("        <DataArray type=\"Int8\" Name=\"PatchIDs\" Format=\"ascii\">\n")
    for patchIdx in range(0, fben, 1):
        stringFormat    = " %i"
        stringFormat    = "         " + stringFormat + "\n"
        fout.write(stringFormat % (fb[patchIdx, fbcn-1]))
    fout.write("        </DataArray>\n")
    fout.write("      </CellData>\n")
    # boundary topology
    fout.write("      <Cells>\n")
    fout.write("        <DataArray type=\"Int64\" Name=\"connectivity\"  Format=\"ascii\">\n")
    offset = 1
    for elements in range(0, fben):
        fout.write("         ")
        # line element
        if (vtksurfacetype == 3):
            for j in range(1, fbcn-1):
                fout.write(" %i" % (fb[elements, j]-1))
        # quadratic edge element
        elif (vtksurfacetype == 21):
            for j in range(1, fbcn-1):
                fout.write(" %i" % (fb[elements, j]-1))
        # bilinear triangle
        elif (vtksurfacetype == 5):
            for j in range(1, fbcn-1):
                fout.write(" %i" % (fb[elements, j]-1))
        # biquadratic triangle
        elif (vtksurfacetype == 22):
            fout.write(" %i" % (fb[elements, offset+0]-1))
            fout.write(" %i" % (fb[elements, offset+1]-1))
            fout.write(" %i" % (fb[elements, offset+2]-1))
            fout.write(" %i" % (fb[elements, offset+3]-1))
            fout.write(" %i" % (fb[elements, offset+5]-1))
            fout.write(" %i" % (fb[elements, offset+4]-1))
        # bilinear quadrilateral
        elif (vtksurfacetype == 9):
            fout.write(" %i" % (fb[elements, offset+0]-1))
            fout.write(" %i" % (fb[elements, offset+1]-1))
            fout.write(" %i" % (fb[elements, offset+3]-1))
            fout.write(" %i" % (fb[elements, offset+2]-1))
        # biquadratic quadrilateral
        elif (vtksurfacetype == 28):
            fout.write(" %i" % (fb[elements, offset+0]-1))
            fout.write(" %i" % (fb[elements, offset+1]-1))
            fout.write(" %i" % (fb[elements, offset+3]-1))
            fout.write(" %i" % (fb[elements, offset+2]-1))
            fout.write(" %i" % (fb[elements, offset+4]-1))
            fout.write(" %i" % (fb[elements, offset+7]-1))
            fout.write(" %i" % (fb[elements, offset+8]-1))
            fout.write(" %i" % (fb[elements, offset+5]-1))
            fout.write(" %i" % (fb[elements, offset+6]-1))
        # trilinear tetrahedron
        elif (vtksurfacetype == 10):
            for j in range(1, fbcn-1):
                fout.write(" %i" % (fb[elements, j]-1))
        # triquadratic tetrahedron
        elif (vtksurfacetype == 24):
            for j in range(1, fbcn-1):
                if j == 6:
                    fout.write(" %i" % (fb[elements, 5]-1))
                elif j == 5:
                    fout.write(" %i" % (fb[elements, 6]-1))
                else:
                    fout.write(" %i" % (fb[elements, j]-1))
        # trilinear hexahedron
        elif (vtksurfacetype == 12):
            fout.write(" %i" % (fb[elements, offset+0]-1))
            fout.write(" %i" % (fb[elements, offset+1]-1))
            fout.write(" %i" % (fb[elements, offset+5]-1))
            fout.write(" %i" % (fb[elements, offset+4]-1))
            fout.write(" %i" % (fb[elements, offset+2]-1))
            fout.write(" %i" % (fb[elements, offset+3]-1))
            fout.write(" %i" % (fb[elements, offset+7]-1))
            fout.write(" %i" % (fb[elements, offset+6]-1))
        # triquadratic hexahedron
        elif (vtksurfacetype == 29):
            fout.write(" %i" % (fb[elements, offset+0]-1))
            fout.write(" %i" % (fb[elements, offset+1]-1))
            fout.write(" %i" % (fb[elements, offset+5]-1))
            fout.write(" %i" % (fb[elements, offset+4]-1))
            fout.write(" %i" % (fb[elements, offset+2]-1))
            fout.write(" %i" % (fb[elements, offset+3]-1))
            fout.write(" %i" % (fb[elements, offset+7]-1))
            fout.write(" %i" % (fb[elements, offset+6]-1))
            fout.write(" %i" % (fb[elements, offset+8]-1))
            fout.write(" %i" % (fb[elements, offset+15]-1))
            fout.write(" %i" % (fb[elements, offset+22]-1))
            fout.write(" %i" % (fb[elements, offset+13]-1))
            fout.write(" %i" % (fb[elements, offset+12]-1))
            fout.write(" %i" % (fb[elements, offset+21]-1))
            fout.write(" %i" % (fb[elements, offset+26]-1))
            fout.write(" %i" % (fb[elements, offset+19]-1))
            fout.write(" %i" % (fb[elements, offset+9]-1))
            fout.write(" %i" % (fb[elements, offset+11]-1))
            fout.write(" %i" % (fb[elements, offset+25]-1))
            fout.write(" %i" % (fb[elements, offset+23]-1))
            fout.write(" %i" % (fb[elements, offset+16]-1))
            fout.write(" %i" % (fb[elements, offset+18]-1))
            fout.write(" %i" % (fb[elements, offset+10]-1))
            fout.write(" %i" % (fb[elements, offset+24]-1))
            fout.write(" %i" % (fb[elements, offset+14]-1))
            fout.write(" %i" % (fb[elements, offset+20]-1))
            fout.write(" %i" % (fb[elements, offset+17]-1))
        fout.write("\n")
    fout.write("        </DataArray>\n")
    # cell locations
    fout.write("        <DataArray type=\"Int64\" Name=\"offsets\"  Format=\"ascii\">\n")
    for points in range(fbcn-2, (fbcn-2)*(fben+1), fbcn-2):
        fout.write("          %i" % (points))
    fout.write("\n        </DataArray>\n")
    # cell types
    fout.write("        <DataArray type=\"Int8\" Name=\"types\"  Format=\"ascii\">\n")
    for points in range(fben):
        fout.write("          "+str(vtksurfacetype)+"\n")
    fout.write("        </DataArray>\n")
    fout.write("      </Cells>\n")
    fout.write("    </Piece>\n")
    fout.write("  </UnstructuredGrid>\n")
    fout.write("</VTKFile>\n")
    fout.close()
    if useCompression:
        # read ASCII vtu file and write raw vtu file
        fin = XMLUnstructuredGridReader(FileName=foutfile)
        # CompressorType: 0 - None, 1 - ZLib, 2 - LZ4, 3 - LZMA
        # DataMode: 0 - Ascii, 1 - Binary, 2 - Appended
        foutbin = XMLUnstructuredGridWriter(CompressorType=1, DataMode=2)
        foutbin.FileName = foutfile
        foutbin.UpdatePipeline()
    ################################################################################################
    # now convert all requested files, looping over all requested times
    for time in range(index0, index1+1, increment):
        # write header
        foutfile = outfile+"-"+str(time)+vtksuffix
        fout = open(foutfile, "w")
        fout.write("<VTKFile type=\"UnstructuredGrid\">\n")
        fout.write("  <UnstructuredGrid>\n")
        ############################################################################################
        # convert space variable
        if (spacename[-2:] == ".X"):
            xfile = spacename
        else:
            xfile = spacename + "-" + str(time) + cheartsuffix
        if not(os.path.isfile(xfile)):
              xfile = xfile + ".gz"
        fx  = np.loadtxt(xfile,  skiprows=1, dtype=float)
        fnn = fx.shape[0]
        fnd = fx.shape[1]
        # if deformed space, then add displacement
        if (len(deformedSpace) == 2):
            dfile   = infolder + deformedSpace[1] + "-" + str(time) + ".D"
            if not(os.path.isfile(dfile)):
                dfile = dfile + ".gz"
            fd  = np.loadtxt(dfile, skiprows=1, dtype=float)
            fx  = fx + fd
        # VTU files are defined in 3D space, so we have to append a zero column for 2D data
        if (fnd == 1):
            print(">>>ERROR: Cannot convert data that lives on 1D domains.")
            return
        elif (fnd == 2):
            z   = np.zeros((fnn,1), dtype=float)
            fx  = np.append(fx, z, axis=1)
        fout.write("    <Piece Name=\""+prefix+"\" NumberOfPoints=\""+str(fnn)+"\" NumberOfCells=\""+str(fen)+"\">\n")
        fout.write("      <Points>\n")
        fout.write("        <DataArray type=\"Float64\" NumberOfComponents=\"3\" Format=\"ascii\">\n")
        for points in range(fx.shape[0]):
            fout.write("          % .16f % .16f % .16f\n" % (fx[points, 0], fx[points, 1], fx[points, 2]))
        fout.write("        </DataArray>\n")
        fout.write("      </Points>\n")
        ############################################################################################
        # convert other variables
        fout.write("      <PointData Scalars=\"scalars\">\n")
        variablesRead   = np.zeros((len(variablenames)), dtype=bool)
        for varIdx in range(0, len(variablenames), 1):
            vfile = infolder + str(variablenames[varIdx]) + "-" + str(time) + ".D"
            if not(os.path.isfile(vfile)):
                vfile = vfile + ".gz"
            fv      = np.loadtxt(vfile,  skiprows=1, dtype=float)
            fvnn    = fv.shape[0]
            if (fv.ndim == 1):
                fvnd = 1
            else:
                fvnd = fv.shape[1]
            # check whether number of scalars matches number of spatial nodes
            if (fnn != fvnn):
                # if it does not match, check if it's cell centered data that's treated separately; else flag an error
                if (fvnn != fen):
                    print(">>>ERROR: Invalid number of nodes for "+str(variablenames[varIdx])+".")
                    return
                else:
                    continue
            variablesRead[varIdx]   = True
            # append zero column if need be
            if (fvnd == 2):
                z       = np.zeros((fvnn,1), dtype=float)
                fv      = np.append(fv, z, axis=1)
                fvnd    = 3
            fout.write("        <DataArray type=\"Float64\" Name=\""+str(variablenames[varIdx])+"\" NumberOfComponents=\""+str(fvnd)+"\" Format=\"ascii\">\n")
            stringFormat    = " % .16f"*fvnd
            stringFormat    = "         " + stringFormat + "\n"
            if(fvnd == 1):
                for points in range(fnn):
                    fout.write(stringFormat % (fv[points]))
            else:
                for points in range(fnn):
                    fout.write(stringFormat % tuple(fv[points, 0:fvnd:1]))
            fout.write("        </DataArray>\n")
        fout.write("      </PointData>\n")
        if (np.any(variablesRead == False)):
            fout.write("      <CellData Scalars=\"scalars\">\n")
        for varIdx in range(0, len(variablenames), 1):
            # check if the variable was already read as point data; if not, it's cell data
            if variablesRead[varIdx]:
                continue
            vfile = infolder + str(variablenames[varIdx]) + "-" + str(time) + ".D"
            if not(os.path.isfile(vfile)):
                vfile = vfile + ".gz"
            fv      = np.loadtxt(vfile,  skiprows=1, dtype=float)
            fvnn    = fv.shape[0]
            if (fv.ndim == 1):
                fvnd = 1
            else:
                print(">>>ERROR: Element-based data appears to have more than one component: "+str(variablenames[varIdx])+".")
                return
            fout.write("        <DataArray type=\"Float64\" Name=\""+str(variablenames[varIdx])+"\" Format=\"ascii\">\n")
            stringFormat    = " % .16f"*fvnd
            stringFormat    = "         " + stringFormat + "\n"
            for points in range(fvnn):
                fout.write(stringFormat % (fv[points]))
            fout.write("        </DataArray>\n")
        if (np.any(variablesRead == False)):
            fout.write("      </CellData>\n")
        ############################################################################################
        # topology
        fout.write("      <Cells>\n")
        fout.write("        <DataArray type=\"Int64\" Name=\"connectivity\"  Format=\"ascii\">\n")
        for elements in range(0, fen):
            fout.write("         ")
            # line element
            if (vtkelementtype == 3):
                for j in range(0, fcn):
                    fout.write(" %i" % (fb[elements, j]-1))
            # quadratic edge element
            elif (vtkelementtype == 21):
                for j in range(0, fcn):
                    fout.write(" %i" % (fb[elements, j]-1))
            # bilinear triangle
            elif (vtkelementtype == 5):
                for j in range(0, fcn):
                    fout.write(" %i" % (ft[elements, j]-1))
            # biquadratic triangle
            elif (vtkelementtype == 22):
                fout.write(" %i" % (ft[elements, 0]-1))
                fout.write(" %i" % (ft[elements, 1]-1))
                fout.write(" %i" % (ft[elements, 2]-1))
                fout.write(" %i" % (ft[elements, 3]-1))
                fout.write(" %i" % (ft[elements, 5]-1))
                fout.write(" %i" % (ft[elements, 4]-1))
            # bilinear quadrilateral
            elif (vtkelementtype == 9):
                fout.write(" %i" % (ft[elements, 0]-1))
                fout.write(" %i" % (ft[elements, 1]-1))
                fout.write(" %i" % (ft[elements, 3]-1))
                fout.write(" %i" % (ft[elements, 2]-1))
            # biquadratic quadrilateral
            elif (vtkelementtype == 28):
                fout.write(" %i" % (ft[elements, 0]-1))
                fout.write(" %i" % (ft[elements, 1]-1))
                fout.write(" %i" % (ft[elements, 3]-1))
                fout.write(" %i" % (ft[elements, 2]-1))
                fout.write(" %i" % (ft[elements, 4]-1))
                fout.write(" %i" % (ft[elements, 7]-1))
                fout.write(" %i" % (ft[elements, 8]-1))
                fout.write(" %i" % (ft[elements, 5]-1))
                fout.write(" %i" % (ft[elements, 6]-1))
            # trilinear tetrahedron
            elif (vtkelementtype == 10):
                for j in range(0, fcn):
                    fout.write(" %i" % (ft[elements, j]-1))
            # triquadratic tetrahedron
            elif (vtkelementtype == 24):
                for j in range(0, fcn):
                    if j == 6:
                        fout.write(" %i" % (ft[elements, 5]-1))
                    elif j == 5:
                        fout.write(" %i" % (ft[elements, 6]-1))
                    else:
                        fout.write(" %i" % (ft[elements, j]-1))
            # trilinear hexahedron
            elif (vtkelementtype == 12):
                fout.write(" %i" % (ft[elements, 0]-1))
                fout.write(" %i" % (ft[elements, 1]-1))
                fout.write(" %i" % (ft[elements, 5]-1))
                fout.write(" %i" % (ft[elements, 4]-1))
                fout.write(" %i" % (ft[elements, 2]-1))
                fout.write(" %i" % (ft[elements, 3]-1))
                fout.write(" %i" % (ft[elements, 7]-1))
                fout.write(" %i" % (ft[elements, 6]-1))
            # triquadratic hexahedron
            elif (vtkelementtype == 29):
                fout.write(" %i" % (ft[elements, 0]-1))
                fout.write(" %i" % (ft[elements, 1]-1))
                fout.write(" %i" % (ft[elements, 5]-1))
                fout.write(" %i" % (ft[elements, 4]-1))
                fout.write(" %i" % (ft[elements, 2]-1))
                fout.write(" %i" % (ft[elements, 3]-1))
                fout.write(" %i" % (ft[elements, 7]-1))
                fout.write(" %i" % (ft[elements, 6]-1))
                fout.write(" %i" % (ft[elements, 8]-1))
                fout.write(" %i" % (ft[elements, 15]-1))
                fout.write(" %i" % (ft[elements, 22]-1))
                fout.write(" %i" % (ft[elements, 13]-1))
                fout.write(" %i" % (ft[elements, 12]-1))
                fout.write(" %i" % (ft[elements, 21]-1))
                fout.write(" %i" % (ft[elements, 26]-1))
                fout.write(" %i" % (ft[elements, 19]-1))
                fout.write(" %i" % (ft[elements, 9]-1))
                fout.write(" %i" % (ft[elements, 11]-1))
                fout.write(" %i" % (ft[elements, 25]-1))
                fout.write(" %i" % (ft[elements, 23]-1))
                fout.write(" %i" % (ft[elements, 16]-1))
                fout.write(" %i" % (ft[elements, 18]-1))
                fout.write(" %i" % (ft[elements, 10]-1))
                fout.write(" %i" % (ft[elements, 24]-1))
                fout.write(" %i" % (ft[elements, 14]-1))
                fout.write(" %i" % (ft[elements, 20]-1))
                fout.write(" %i" % (ft[elements, 17]-1))
            fout.write("\n")
        fout.write("        </DataArray>\n")
        # cell locations
        fout.write("        <DataArray type=\"Int64\" Name=\"offsets\"  Format=\"ascii\">\n")
        for points in range(fcn, fcn*(fen+1), fcn):
            fout.write("          %i" % (points))
        fout.write("\n        </DataArray>\n")
        # cell types
        fout.write("        <DataArray type=\"Int8\" Name=\"types\"  Format=\"ascii\">\n")
        for points in range(fen):
            fout.write("          "+str(vtkelementtype)+"\n")
        fout.write("        </DataArray>\n")
        fout.write("      </Cells>\n")
        fout.write("    </Piece>\n")
        fout.write("  </UnstructuredGrid>\n")
        fout.write("</VTKFile>\n")
        fout.close()
        if useCompression:
            # read ASCII vtu file and write raw vtu file
            fin = XMLUnstructuredGridReader(FileName=foutfile)
            # CompressorType: 0 - None, 1 - ZLib, 2 - LZ4, 3 - LZMA
            # DataMode: 0 - Ascii, 1 - Binary, 2 - Appended
            foutbin = XMLUnstructuredGridWriter(CompressorType=1, DataMode=2)
            foutbin.FileName = foutfile
            foutbin.UpdatePipeline()
    print("################################################################################################")

if __name__ == "__main__":
    main()
