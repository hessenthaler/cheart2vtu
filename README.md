# README #

### What is this repository for? ###

* python converter script
* Converts CHeart data/mesh files (\*.D, \*.X, \*.T, \*.B) to VTU file format

### How do I get set up? ###

For the converter script, you need:

* Paraview-python
* Numpy

To visualize the converted VTU files, you need:

* ParaView

Simply download this repository by using the command:

`cd destinationFolder/`
`git clone https://bitbucket.org/hessenthaler/cheart2vtu`

Print the help by running the script using the command:

`python cheart2vtu.py`

### Features ###

The script can read \*.D and compressed \*.D.gz files.

Implemented element types are:

* bilinear triangles
* biquadratic triangles
* bilinear quadrilaterals
* biquadratic quadrilaterals
* trilinear hexahedra
* triquadratic hexahedra
* trilinear tetrahedra
* triquadratic tetrahedra

### Example ###

This repository ships with example \*.D.gz files from a fluid-structure interaction benchmark simulation [Hessenthaler, Röhrle, Nordsletten (2016)].
To convert the example files run the following commands for the fluid domain:

`python cheart2vtu.py vtu fluidq 10000 10000 1 meshes/domainF_quad_FE.T meshes/domainF_quad_FE.B data FluidSpace Vel Wel phi`

`python cheart2vtu.py vtu fluidl 10000 10000 1 meshes/domainF_lin_FE.T meshes/domainF_lin_FE.B data ../meshes/domainF_lin_FE.X FluidPres`

For the solid domain in the undeformed configuration:

`python cheart2vtu.py vtu solidq0 10000 10000 1 meshes/domainS_quad_FE.T meshes/domainS_quad_FE.B data SolidSpace SolVel Disp CauchyStressS`

`python cheart2vtu.py vtu solidl0 10000 10000 1 meshes/domainS_lin_FE.T meshes/domainS_lin_FE.B data ../meshes/domainS_lin_FE.X SolidPres`

For the solid domain in the deformed configuration:

`python cheart2vtu.py vtu solidq 10000 10000 1 meshes/domainS_quad_FE.T meshes/domainS_quad_FE.B data SolidSpace+Disp SolVel Disp CauchyStressS`

For the interface domain:

`python cheart2vtu.py vtu interfaceq 10000 10000 1 meshes/lm_quad_FE.T meshes/lm_quad_FE.B data BndrySpace LMult`

The `vtu/` folder also contains a state file for ParaView (Linux and MacOS versions) that demonstrates how VTU files can be visualized.

### Limitations ###

No mapping routines have been implemented so far and we currently cannot map, e.g., quadratic and linear topologies.
Thus, there have to be separate spatial variables for each topology, if Taylor-Hood elements are used.

The exception is that element-based data on discontinuous constant topologies can be provided as well under the following assumptions:

* The number of element-based data values has to coincide with the number of elements.
* The number of element-based data values cannot be the same as the number of nodes in the mesh.

### Who do I talk to? ###

* Andreas Hessenthaler

### Known issues ###

In some cases, your `Paraview-python` library might not be detected correctly. Try adding the following to your `~/.bashrc`:

`export LD_LIBRARY_PATH=/usr/lib/python2.7/dist-packages/paraview/:$LD_LIBRARY_PATH`
`export PYTHONPATH=/usr/lib/python2.7/dist-packages/paraview/:$PYTHONPATH`

Note: You might need to adjust paths to match your installation.
